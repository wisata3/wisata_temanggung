-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Jun 2022 pada 13.19
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wisata`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE `artikel` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id`, `nama`, `deskripsi`, `gambar`, `nilai`, `tag`, `map`, `created_at`, `updated_at`) VALUES
(16, 'Curug Lawe', 'Wisata Temanggung yang pertama adalah Curug Lawe. Curug Lawe adalah wisata air terjun yang memiliki pemandangan sangat indah.\n\nUntuk mencapai Curug Lawe, Moms harus melewati jalanan menanjak yang agak licin karena lokasi wisata Temanggung ini terbilang masuk ke dalam kawasan hutan.\n\nTapi saat berjalan menapaki jalanan tersebut, Moms akan dimanjakan dengan pepohonan hijau serta suara-suara burung serta hewan lainnya yang bisa menghilangkan penatmu.\n\nDalam bahasa Jawa, Lawe artinya benang-benang halus. Jadi nantinya ketika Moms melihat air terjun ini, Moms akan terpana dengan percikan airnya yang mirip dengan untaian benang.\n\nCurug Lawe berlokasi di Desa Muncar, Kecamatan Gemawang. Untuk harga tiket masuknya hanya Rp6.000. Tapi jika membawa kendaraan pribadi, Moms harus membayar uang parkir sekitar Rp5.000 hingga Rp10.000.', 'https://cdn-cas.orami.co.id/parenting/images/Curug_Lawe_Temanggung_instagram.com_exploremag.width-800.jpg', '7.5', 'Curug', 'Tanjungsari, Duren, Bejen, Kabupaten Temanggung, Jawa Tengah 56258', '2022-06-28 10:55:59', '2022-06-28 11:13:45'),
(17, 'Wisata Alam Posong', 'Wisata Temanggung yang terakhir adalah Wisata Alam Posong yang terkenal sebagai golden sunrise dan golden sunsetnya karena pemandangan matahari terbit dan tenggelam di sini sangat cantik.\n\nBiasanya para wisatawan akan datang pada sore hari saat ingin melihat sunset, tapi jika ingin melihat sunrise, kebanyakan wisatawan akan memutuskan untuk mencari penginapan di sekitarnya. Meski ada juga yang datang sebelum pukul 04.30 alias sebelum matahari terbit.\n\nUntuk dapat sampai di puncak tertinggi atau tempat melihat matahari terbit dan tenggelam, Moms harus mendaki terlebih dahulu sepanjang 3,5 km dari bawah.\n\nSelain melihat pemandangan matahari terbit dan tenggelam, di Wisata Alam Posong ini, Moms juga dapat berfoto di beberapa spot kece yang memang disediakan oleh pihak pengelola. Ada spot tempat duduk ayunan, sayap kupu-kupu, dan lain sebagainya.\n\nTempat wisata Temanggung ini berlokasi di Wisata Alam Posong, tepatnya di Desa Tlahap, Kledung. Wisata Alam Posong ini hanya mematok biaya sebesar Rp10.000 sebagai tiket masuknya.', 'https://cdn-cas.orami.co.id/parenting/images/Wisata_Alam_Posong_Temanggung_instagram.com_ay.width-800.jpg', '8.0', 'Alam', 'Area Sawah, Tlahap, Kecamatan Kledung, Kabupaten Temanggung, Jawa Tengah 56264', '2022-06-28 11:01:47', '2022-06-28 11:08:34'),
(18, 'Kledung Pass', 'Kledung Pass adalah objek wisata Temanggung yang awalnya merupakan perkebunan teh dan tembakau. Tapi kemudian beralih fungsi menjadi lokasi wisata alam yang menyajikan pemandangan alam yang indah.\n\nDi sini terdapat kolam dengan latar pemandangan bukit sekitar yang asri. Selain itu juga ada tempat untuk berkemah.\n\nWisata Temanggung ini punya fasilitas yang sangat lengkap sekali. Mulai dari mushola, toilet, rumah makan, hingga penginapan buat Moms yang ingin menginap di hotel.\n\nKledung Pass berlokasi di Jalan Raya Parakan, Wonosobo, Kledung, Temanggung. Dari pusat kota jaraknya hanya sekitar 21 km. Untuk tiket masuknya gratis alias tidak dipungut biaya.', 'https://cdn-cas.orami.co.id/parenting/images/Kledung_Pass_Temanggung_instagram.com_explorem.width-800.jpg', '7.0', 'Danau', 'Kledung, Kecamatan Kledung, Kabupaten Temanggung, Jawa Tengah 56264', '2022-06-28 11:07:20', '2022-06-28 11:09:05'),
(19, 'Hutan Pinus Sigrowong', 'Wisata Temanggung selanjutnya yang menyajikan pemandangan alam indah adalah Hutan Pinus Sigrowong. Hutan ini merupakan hutan pinus yang kemudian difungsikan sebagai tempat wisata Temanggung.\n\nAda ribuan pohon pinus yang ada di area perbukitan. Selain bisa merasakan aroma khas hutan pinus dan pemandangan hijaunya, Moms juga bisa melihat pohon kopi yang juga ditanam di sekitar area tersebut.\n\nJika suka foto-foto, di tempat wisata Temanggung ini ada beberapa spot foto yang sudah disiapkan oleh pengelola. Salah satu spot foto yang menjadi favorit adalah perahu merah putih.\n\nSpot perahu merah putih ini terbuat dari perahu yang dibuat dari kayu dan ban sepeda yang ditata apik sehingga menyerupai bentuk perahu. Pada bagian ujungnya diletakkan bendera merah putih.\n\nDaya tarik lainnya dari Hutan Pinus Sigrowong adalah adanya Gua Sigrowong yang bisa dijumpai di sini. Gua ini berkedalaman kurang lebih 7 meter. Konon dulunya gua ini adalah tempat persembunyian semasa agresi militer Belanda.\n\nHutan Pinus Sigrowong berlokasi di Kandangan Rowo Seneng, Gesing, Kabupaten Kandangan. Untuk tiket masuknya hanya Rp5.000 saja.', 'https://cdn-cas.orami.co.id/parenting/images/Hutan_Pinus_Sigrowong_www.indeksberita.com.width-800.jpg', '8.0', 'Hutan', 'Kandangan Rowo Seneng, Hutan, Gesing, Kandangan, Kabupaten Temanggung, Jawa Tengah 56281', '2022-06-28 11:12:21', '2022-06-28 11:12:21'),
(20, 'Hutan Walitis', 'Hutan Wakitis adalah wisata Temanggung yang lokasinya berada di dalam kawasan Hutan Rasamala. Tepatnya di Desa Jetis, Tanggulamon, Selopampang.\n\nDi dalam Hutan Walitis ada banyak pohon yang berukuran sangat besar dan usianya sudah puluhan tahun. Beberapa di antaranya bahkan ada yang disebut sebagai pohon terbesar di lereng Gunung Sumbing dan Sindoro.\n\nPohon-pohon yang ukurannya besar itu memiliki tinggi hingga mencapai 30 meter dengan lingkar diameter batangnya yang bisa mencapai 7,5 meter. Jadi jika ingin memeluk pohon tersebut, dibutuhkan setidaknya 6 orang dewasa yang tangannya saling direntangkan.\n\nMenurut kepercayaan masyarakat setempat, pohon-pohon besar tersebut dipercaya berasal dari tongkat Ki Ageng Makukuhan yang merupakan pengikut Wali Songo. Tongkat tersebut ditancapkan ke tanah dan menjadi pohon yang ukurannya sangat besar.\n\nSelain pohon-pohon besar tersebut juga ada tanaman Rasamala yang konon tidak mempan terhadap api.\n\nUntuk bisa masuk ke dalam Hutan Walitis, Moms bisa masuk secara gratis. Tapi tidak ada lahan parkir yang disediakan sehingga jika ingin memarkirkan kendaraan, Moms harus memarkirkannya di dekat rumah penduduk sekitar.', 'https://cdn-cas.orami.co.id/parenting/images/Hutan_Walitis_Temanggung_instagram.com_wisata_.width-800.jpg', '7.5', 'Hutan', 'Tanggulanom, Selopampang, Tanggulanom, Selopampang, Kabupaten Temanggung, Jawa Tengah 56262', '2022-06-28 11:13:32', '2022-06-28 11:13:32'),
(21, 'Alam Sewu', 'Wisata Temanggung Alam Sewu sangat populer dengan keindahan alamnya. Saking terkenalnya, wisata ini dijuluki dengan nama negeri di atas awannya Temanggung.\n\nHal ini dikarenakan jika Moms berada di puncuk atas bukitnya, Moms bisa melihat kabut yang cukup tebal di pagi hari.\n\nDitambah lagi adanya pemandangan perkebunan tembakau serta gunung-gunung yang ada di sekitaran Alam Sewu. Mulai dari Gunung Ungaran, Gunung Sindoro, Gunung Kendil, dan masih banyak lagi.\n\nTak heran kalau pemandangannya bisa bikin Moms berdecak kagum. Untuk sampai ke bagian puncak, Moms harus berjalan cukup jauh dan menanjak sehingga dianjurkan untuk menggunakan sepeda motor.\n\nJika ingin berkemah, di sini ada spot khusus berkemah. Jadi saat Moms membuka mata di pagi hari bisa langsung melihat matahari terbit yang terlihat sangat cantik.\n\nWisata Temanggung Alam Sewu ini lokasinya berada sekitar 35 km dari pusat kota Temanggung. Tepatnya berada di Desa Pringsweu, Kecamatan Ngadirejo. Untuk tiket masuknya hanya Rp5.000 saja per orangnya.', 'https://cdn-cas.orami.co.id/parenting/images/Alam_Sewu_triptommy.blogspot.com.width-800.jpg', '8.0', 'Alam', 'Area Sawah, Giripurno, Kecamatan Ngadirejo, Kabupaten Temanggung, Jawa Tengah 56255', '2022-06-28 11:15:39', '2022-06-28 11:15:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2022_06_27_165136_create_artikel_table', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
