import { createRouter, createWebHistory } from 'vue-router';
// import HomeView from '../views/HomeView.vue'
import Content from '../views/Content.vue';
import Admin from '../views/Admin.vue';
import Create from '../views/Create.vue';
import Edit from '../views/Edit.vue';


const routes = [
  {
    path: '/',
    name: 'Content',
    component: Content
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin
  },
  {
    path: '/create',
    name: 'create',
    component: Create
  },
  {
    path: '/edit',
    name: 'Edit',
    component: Edit
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
