<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/admin', 'ArtikelController@index');
$router->post('/admin', 'ArtikelController@store');
$router->get('/admin/{id}', 'ArtikelController@show');
$router->put('/admin/{id}', 'ArtikelController@update');
$router->delete('/admin/{id}', 'ArtikelController@destroy');
