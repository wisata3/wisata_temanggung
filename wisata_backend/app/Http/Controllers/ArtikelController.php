<?php

namespace App\Http\Controllers;

use App\Artikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArtikelController extends Controller
{
    public function index()
    {
        $artikels =Artikel::all();

        return response()->json([
            'success' => true,
            'message' =>'List Semua Artikel',
            'data'    => $artikels
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'      => 'required',
            'deskripsi' => 'required',
            'gambar'    => 'required',
            'nilai'     => 'required',
            'tag'       => 'required',
            'map'       => 'required',

        ]);

        if ($validator->fails()) {

            return response()->json([
                'success' => false,
                'message' => 'Semua Kolom Wajib Diisi!',
                'data'   => $validator->errors()
            ],401);

        } else {

            $Artikel = Artikel::create([
                'nama'      => $request->input('nama'),
                'deskripsi' => $request->input('deskripsi'),
                'gambar'    => $request->input('gambar'),
                'nilai'     => $request->input('nilai'),
                'tag'       => $request->input('tag'),
                'map'       => $request->input('map'),
            ]);

            if ($Artikel) {
                return response()->json([
                    'success' => true,
                    'message' => 'Artikel Berhasil Disimpan!',
                    'data' => $Artikel
                ], 201);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Artikel Gagal Disimpan!',
                ], 400);
            }

        }
    }

    public function show($id)
    {
        $Artikel = Artikel::find($id);

        if ($Artikel) {
            return response()->json([
                'success'   => true,
                'message'   => 'Detail Artikel!',
                'data'      => $Artikel
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Artikel Tidak Ditemukan!',
            ], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama'      => 'required',
            'deskripsi' => 'required',
            'gambar'    => 'required',
            'nilai'     => 'required',
            'tag'       => 'required',
            'map'       => 'required',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'success' => false,
                'message' => 'Semua Kolom Wajib Diisi!',
                'data'   => $validator->errors()
            ],401);

        } else {

            $Artikel = Artikel::whereId($id)->update([
                'nama'      => $request->input('nama'),
                'deskripsi' => $request->input('deskripsi'),
                'gambar'    => $request->input('gambar'),
                'nilai'     => $request->input('nilai'),
                'tag'       => $request->input('tag'),
                'map'       => $request->input('map'),
            ]);

            if ($Artikel) {
                return response()->json([
                    'success' => true,
                    'message' => 'Artikel Berhasil Diupdate!',
                    'data' => $Artikel
                ], 201);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Artikel Gagal Diupdate!',
                ], 400);
            }

        }
    }

    public function destroy($id)
    {
        $Artikel = Artikel::whereId($id)->first();
            
        $Artikel->delete();

            if ($Artikel) {
                return response()->json([
                    'success' => true,
                    'message' => 'Artikel Berhasil Dihapus!',
                ], 200);
            }

    }
}