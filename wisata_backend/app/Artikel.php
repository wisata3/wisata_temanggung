<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    /**
     * @var string
     */
    protected $table = 'artikel';

    /**
     * @var array
     */
    protected $fillable = [
        'nama',
        'deskripsi',
        'gambar',
        'nilai',
        'tag',
        'map',
    ];
}